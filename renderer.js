// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.


const version = '1'

const host = 'https://sykiks.com'
const senderId = "265219963074" 


// const host = 'http://localhost:3000'
// const host = 'https://sykiks-mvp.firebaseapp.com'
// const senderId = "993072186456" 



const { ipcRenderer, remote } = require ('electron')

let mainWindow = remote.getCurrentWindow()

const {
  START_NOTIFICATION_SERVICE,
  NOTIFICATION_SERVICE_STARTED,
  NOTIFICATION_SERVICE_ERROR,
  NOTIFICATION_RECEIVED,
  TOKEN_UPDATED,
} = require ('electron-push-receiver/src/constants')

// Listen for service successfully started
ipcRenderer.on(NOTIFICATION_SERVICE_STARTED, (_, token) => {
  var webview = document.getElementById('content');
  let ts = Date.now()
  webview.src = `${host}/login?desktop_token=${encodeURIComponent(token)}&ts=${ts}$version=${version}`
  
  // console.log('service successfully started', token)
})


// Handle notification errors
ipcRenderer.on(NOTIFICATION_SERVICE_ERROR, (_, error) => {
  // console.log('notification error', error)
})

// Send FCM token to backend
var webview = document.getElementById('content');
ipcRenderer.on(TOKEN_UPDATED, (_, token) => {
  webview.src = `${host}/login?desktop_token=${encodeURIComponent(token)}`
})





// Display notification
ipcRenderer.on(NOTIFICATION_RECEIVED, (_, serverNotificationPayload) => {
    // if(!serverNotificationPayload.data)
    mainWindow.restore()  
    mainWindow.show()
    mainWindow.flashFrame(true)
  
})

ipcRenderer.send(START_NOTIFICATION_SERVICE, senderId)


// let timeout= 3 * 60 * 1000
// let timeout= 10 * 1000

// setInterval(function () {
//   var webview = document.getElementById('content');
//   webview.executeJavaScript(`Iamawake()`)  
// }, timeout)


document.addEventListener("keydown", function (e) {
  if (e.which === 123) {
    var webview = document.getElementById('content');
    webview.openDevTools(); 
    // mainWindow.openDevTools()
    
  } else if (e.which === 116) {
    location.reload();
  }
});