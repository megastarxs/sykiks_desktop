const { app, Menu, Tray, BrowserWindow } = require('electron')
var os = require("os");
const { setup: setupPushReceiver } = require('electron-push-receiver');

const path = require('path')
const url = require('url')

let mainWindow

require('electron-reload')(__dirname);


function createWindow () {
  mainWindow = new BrowserWindow({ useContentSize: true,width:1000,height:650, resizable:false, icon: __dirname + '/build/favicon.ico'})
  mainWindow.setMenu(null);

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  setupPushReceiver(mainWindow.webContents);

  let systrayIcon = 'tray.png'
  if (os.platform() == "win32") 
    systrayIcon = 'tray_win.png'
  var tray = new Tray(path.join(__dirname, 'build', systrayIcon))

  var contextMenu = Menu.buildFromTemplate([
    {
      label: 'Open Sykiks', click: function () {
        mainWindow.show()
      }
    },
    {
      label: 'Exit', click: function () {
        app.isQuiting = true
        app.quit()
      }
    }
  ])

  tray.on('click', function (event) {
    mainWindow.show();
  });

  //tray.setHighlightMode('always');
  tray.setToolTip('Sykiks')

  tray.setPressedImage(path.join(__dirname, 'build', 'tray_press.png'))
  tray.setContextMenu(contextMenu)
  mainWindow.tray = tray

  mainWindow.on('minimize', function (event) {
    event.preventDefault();
    mainWindow.hide();
  });

  mainWindow.on('close', function (event) {
    if (!app.isQuiting) {
      event.preventDefault();
      mainWindow.hide();
    }
    return false;
  });

  mainWindow.on('closed', function () {
    mainWindow = null
  })


}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

